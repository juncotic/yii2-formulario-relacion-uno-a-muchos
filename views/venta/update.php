<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Venta */

$this->title = 'Actualizar Venta: ' . $model->venta->ventaId;
$this->params['breadcrumbs'][] = ['label' => 'Ventas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->venta->ventaId, 'url' => ['view', 'id' => $model->venta->ventaId]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="venta-update">

    <h2><?= Html::encode($this->title) ?></h2>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
