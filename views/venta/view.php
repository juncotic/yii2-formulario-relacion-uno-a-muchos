<?php

use yii\helpers\Html;
use yii\widgets\DetailView;


/* @var $this yii\web\View */
/* @var $model app\models\Venta */

$this->title = $model->ventaId;
$this->params['breadcrumbs'][] = ['label' => 'Ventas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="venta-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Actualizar', ['update', 'id' => $model->ventaId], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Borrar', ['delete', 'id' => $model->ventaId], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Está seguro que desea eliminar el item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'ventaId',
            'cliente',
            'fecha',
        ],
    ]) ?>

    <table class="table table-striped table-bordered detail-view">
    <tr><th>Cantidad</th><th>Descripción</th></tr>
    <?php
        foreach($model->productos as $producto){
    ?>
           <tr>
           <td width="30"><?= $producto->cant?></td>
           <td><?= $producto->descripcion ?></td>
           </tr>
    <?php
     }
    ?>
     </table>

</div>
